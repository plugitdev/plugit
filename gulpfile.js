var gulp = require("gulp"),
  sass = require("gulp-sass"),
  rename = require("gulp-rename"),
  cssmin = require("gulp-cssnano"),
  prefix = require("gulp-autoprefixer"),
  plumber = require("gulp-plumber"),
  sassLint = require("gulp-sass-lint"),
  uglify   = require("gulp-uglify"),
  filter   = require("gulp-filter"),
  sourcemaps = require("gulp-sourcemaps"),
  ts = require("gulp-typescript");
  glob = require("gulp-sass-glob");

// BUILD OPTIONS
// ---------------
var buildpath = "./dist";
var stylesheetsFolder = "/public/stylesheets";
var sassOptions = {
  outputStyle: "expanded",
  includePaths: ["node_modules"],
  esModuleInterop: true,
};
var prefixerOptions = {};
var tsProject = ts.createProject("tsconfig.json");

// BUILD TASKS
// ------------
gulp.task("typescript-dev", () => {
    return gulp.src('src/**/*.ts')
    .pipe(tsProject())
    .pipe(gulp.dest(buildpath));
});

gulp.task("typescript-defs", () => {
  return gulp.src('src/**/*.ts')
  .pipe(tsProject())
  .pipe(filter(['**.d.ts', '!*/**.ts']))
  .pipe(gulp.dest(buildpath));
})

gulp.task("typescript-uglify", () => {
    return gulp.src('src/**/*.ts')
    .pipe(tsProject())
    .pipe(filter(['**', '!*/**.d.ts']))
    .pipe(uglify())
    .pipe(gulp.dest(buildpath));
  })

gulp.task('js', () => {
    return gulp.src("src/**/*.js").pipe(gulp.dest(buildpath));
})

gulp.task('modules', () => {
    return gulp.src("./modules/**/*.*").pipe(gulp.dest(buildpath + "/modules"));
})

gulp.task('modules-uglify', () => {
  return gulp.src("./modules/**/*.*").pipe(gulp.dest(buildpath + "/modules")).pipe(filter(['**/*.js'])).pipe(uglify()).pipe(gulp.dest(buildpath + "/modules"));
})

gulp.task('views', () => {
    return gulp.src("./src/views/**/*").pipe(gulp.dest(buildpath + "/views"));
})

gulp.task('public', () => {
    return gulp.src("./public/**/*.*").pipe(gulp.dest(buildpath + "/public"));
})

gulp.task("scss", () => {
  return gulp
    .src("./src/scss/*.scss")
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(glob())
    .pipe(sass(sassOptions))
    .pipe(prefix(prefixerOptions))
    .pipe(rename("main.css"))
    .pipe(gulp.dest(buildpath + stylesheetsFolder))
    .pipe(cssmin())
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest(buildpath + stylesheetsFolder));
});

exports.dev = gulp.series(['typescript-dev', 'scss', 'js', 'modules', 'views', 'public'])
exports.prod = gulp.series(['typescript-defs', 'typescript-uglify', 'scss', 'js', 'modules-uglify', 'views', 'public'])