var {settings} = require('../src/util/constants/tableNames');

exports.seed = (knex) => {
  return knex(settings)
    .del()
    .then(() => {
      return knex(settings).insert([
        {
          id: 1,
          token: undefined,
          prefix: "!",
          clientid: undefined,
          clientsecret: undefined,
          playing: "The Modular Discord Bot.",
          SESSION_SECRET: undefined
        },
      ]);
    });
};
