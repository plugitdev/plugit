![Plugit Logo](https://gitlab.com/plugitdev/plugit/-/raw/master/public/logo.png)
### Plugit Discord Bot 
We are using Discord.JS, Express, and Knex.JS to implement a discord bot that allows users to install modules and custom commands.

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

You can add the bot to your server using the [Invite Link](https://discord.com/oauth2/authorize?client_id=355715029296742403&scope=bot&permissions=8)

### Todolist
- [ ] Update Module generator to use the new format and features, maybe include more examples.
- [ ] Update to latest version of Discord.JS

For more information regarding how you can contribute to the repository check out our [Contributing Guidlines]()  and [Active Issues]()
## Setup
Setting up the bot is easy. First of all clone the repo:
`git clone git@gitlab.com:MarleyPlant/Plugit`

Create `.env` file and inside add the following configuration options, you can find these on the [dicord developer portal](https://discord.com/developers/applications).
```
CLIENT_ID=''
CLIENT_SECRET=''
SESSION_SECRET=''
```

Starting the for the first time is as easy as:
`npm run setup && npm start`

after this you must sign in on the web interface at [http://localhost:8080]().
## Creating Your Own Modules.
For creating your own modules and modules Plugit exposes certain classes to you as well as providing some utility functions.
To get started creating a module go to your modules folder in the plugit root. Here you can create a new folder for your module and an javascript file for the actual code of the module.

in this file you want to export the following, this is a basic plugit module structure, there are easier ways of defining this you can look at the generator below to get an idea of how that works.

the basic structure is an object containing two others objects names commands and events, these objects will then contain the commands and events that plugit should listen out for and what it should do if the command or event is fired.
```js
module.exports = {
    commands: {}
    
    events: {}
}
```

inside of these lists you can put command or event objects, for a full list of these and examples click here.
```js
example: {
    name: 'example', //The Name of your command how it should be typed into the discord chat - the prefix.
    parameters: {
        params: [
            "(name)",
            "(role)",
        ],
        required: true, //Will the Main Function stil run if no arguments are supplied?
    },
    help: '', //Text that is shown under the command in the help command.
    main: function(bot, db, msg) //Should be a function that contains the bulk of your code. See below for examples.
}
```
here is a basic command, if you place this as an object inside of the commands array plugit will then automatically load this command when it loads your module, Its pretty self explanatory. any questions? dont be afraid to ask on our discord.


The Main function is exposed 3 variables which can be referenced in the discord.js & and knex documents under [bot](https://discord.js.org/#/docs/main/stable/class/Client), [db](http://knexjs.org/#Builder), [msg](https://discord.js.org/#/docs/main/stable/class/Message)

##Yeoman Generator
```bash
npm install -g yeoman generator-plugit-module
yo plugit-module
```
This will create two folders called events and commands as well as an index file.

The index files will collect all the other files from the folder into one import.

The main index file will collect both the events and commands into a format that plugit can understand.

To create a custom command.

```bash
cd commands
mv exampleCommand.js (yourCommand Name)
```

then anywhere inside of the index.js file where its references to exampleCommand Change it to the name of your command.

Each command takes certain parameters so that plugit knows how to handle it properly.

## Database
The database will be using Postgres and Knex.JS.


Database Relationship ER Diagram can be found [Here](https://app.lucidchart.com/invitations/accept/af12fac3-fb13-435f-a70c-f407b0b7a554).


### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.
