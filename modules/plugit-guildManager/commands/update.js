const tableNames = require("../../../util/constants/tableNames");

module.exports = {
  name: "update",
  parameters: {
    params: [],
    required: false,
  },
  help: "Update Server Information And Icon On Bots Dashboard.",
  main: function (bot, db, msg, guildManager) {
    db.knex(tableNames.server)
      .where({ discord_ID: msg.guild.id })
      .first()
      .then((server) => {
        if (!server) {
          guildManager.createServer(msg.guild, db.knex);
          msg.channel.send("Created Guild Entry In Database!");
        } else {
          guildManager.updateServer(msg.guild, db.knex);
          msg.channel.send("Updated Guild In Database.");
        }
      });
  },
};
