require("dotenv").config();
import {
  MessageEmbed,
  Client,
  Constants,
  Message,
  Guild,
  ClientEvents,
} from "discord.js";
import {
  databaseConnection,
  ModuleManager,
  args as argsParser,
  logger,
  guildManager,
} from "./util";

import * as chalk from "chalk";
import { ModuleEvent, PlugitModule } from "./interfaces";
let developersURL = chalk.green("https://discord.com/developers/applications");

function stringStartsWithPrefix(msg: Message, prefix: string) {
  return msg.content.indexOf(prefix) !== -1;
}

export class Plugitbot {
  public db: databaseConnection;
  public bot: Client;
  public ModuleManager: ModuleManager;
  public guildManager: guildManager;
  public playing: string;

  constructor() {
    this.db = new databaseConnection();
    this.bot = new Client();
    this.ModuleManager = new ModuleManager();

    this.guildManager = new guildManager(this.db);

    this.initDiscordJS();

    this.helpCommand = this.helpCommand.bind(this);
  }

  public start() {
    if (!this.bot) {
      this.bot = new Client();
      this.initDiscordJS();
    }
  }

  public restart() {
    this.bot.destroy();
    this.start();
  }

  initDiscordJS() {
    this.setupOnReady();
    this.setupCommandHandler();
    this.setupEvents();
    this.startBot();
  }

  tryLogin(token: string) {
    this.bot.login(process.env.token || token).catch((error: any) => {
      if (error.code == "TOKEN_INVALID")
        logger.error(
          `Discord Rejected token, Invalid Token Provided. please get a valid token from ${developersURL}`
        );
    });
  }

  setupEventHandlers() {
    let plugitEvent;

    for (plugitEvent in this.ModuleManager.events) {
      let DJSEvent = plugitEvent.event;
      if (DJSEvent == Constants.Events.CLIENT_READY) return;

      this.bot.on(DJSEvent, (arg1: any) => {
        DJSEvent.main(this.bot, this.db, arg1);
      });
    }
  }

  setupOnReady() {
    this.bot.on(Constants.Events.CLIENT_READY, () => {
      let numberOfGuilds: number = this.bot.guilds.cache.array().length;
      let numberOfUsers: number = this.bot.users.cache.array().length;

      logger.log(`Logged in as ${chalk.yellow(this.bot.user.tag)}!`);
      logger.log(
        `Connected to ${chalk.yellow(
          numberOfGuilds
        )} guilds, serving ${chalk.green(numberOfUsers)} users.`
      );

      this.bot.user.setActivity(process.env.playing || this.playing);
      this.ModuleManager.loadModules();
      this.setupHelpCommand();
      // this.guildManager.updateAllServers(this.bot);

      //Handle events
      if (this.ModuleManager.events["ready"]) {
        this.ModuleManager.events["ready"].main(this.bot, this.db);
      }

      setTimeout(() => {
        this.setupEventHandlers();
        this.ModuleManager.listModules();
      }, 3000);
    });
  }

  setupHelpCommand() {
    //Help Command
    this.ModuleManager.addCommand({
      name: "help",
      help: "Display A List of commands And Their Features!",
      parameters: {
        params: [],
        required: false,
      },
      main: this.helpCommand,
    });
  }

  private helpCommand(bot: Client, database: databaseConnection, msg: Message) {
    let embed = new MessageEmbed().setTitle("Bot commands");
    for (let command in this.ModuleManager.commands) {
      this.ModuleManager.addHelpFieldToEmbed(embed, command);
    }
    msg.channel.send({ embed });
  }

  setupEvents() {
    this.bot.on("guildCreate", this.guildManager.guildCreate);
    this.bot.on("guildUpdate", this.guildManager.guildUpdate);
  }

  private setupCommandHandler() {
    //Handle commands
    this.bot.on(Constants.Events.MESSAGE_CREATE, async (msg: Message) => {
      if (msg.author.bot) return; //If message is from a bot ignore.
      let prefix = this.ModuleManager.prefix;

      if (stringStartsWithPrefix(msg, prefix)) {
        var { command, args } = argsParser.parse(msg, prefix);

        if (command in this.ModuleManager.commands) {
          if (process.env.delete_commands == "true") msg.delete();
          if (
            await this.ModuleManager.doesCommandRequireParams(command, args)
          ) {
            let reply = `You didn't provide any arguments, ${msg.author}!`;

            if (this.ModuleManager.commands[command].parameters.params) {
              reply += this.ModuleManager.getProperUsageText(command);
            }

            return msg.channel.send(reply);
          } else {
            this.ModuleManager.run(this, msg, command);
          }
        }
      }

      if (this.ModuleManager.events["message"]) {
        this.ModuleManager.events["message"].main(this.bot, this.db, msg);
      }
    });
  }

  private startBot() {
    this.db.getSettings(async (data: any) => {
      var { token, prefix, clientid, clientsecret } = data;
      this.ModuleManager.prefix = prefix;
      this.playing = data.playing;

      if (!process.env.CLIENT_ID && !clientid) {
        logger.error(
          `Please get a clientID from ${developersURL} then set using '.env' file`
        );
      }

      if (!process.env.CLIENT_SECRET && !clientsecret) {
        logger.error(
          `Please get a Client Secret from ${developersURL} then set using '.env' file`
        );
      }

      if (!process.env.token && !token) {
        logger.error(
          `Please get a token from ${developersURL} then set using '.env' file or by using the Web UI.`
        );
        return;
      }

      this.tryLogin(token);
    });
  }
}
