module.exports = {
    isEmpty: require('./isEmpty'),
}
export { isAuth } from './isAuth';
export {stringStartsWithPrefix} from './stringStartsWithPrefix';