import { Message } from "discord.js";

export function stringStartsWithPrefix(msg: Message, prefix: string) {
  return (msg.content.indexOf(prefix) !== -1);
};
