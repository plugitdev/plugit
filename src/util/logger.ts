const sig = require("./constants/signature");
const chalk = require('chalk');


function log (text) {
 console.log(`${chalk.green(sig)} ${text}`);
}

function chalkifyStatusCode(code) {
  if (code == 200) {
    return chalk.green(code);
  } else {
    return chalk.red(code);
  }
}

export var logger = {
    logo: () => {
        let logo = `                                                                                                        
      R  R                                          
      R  R                                          
      R  R                                          
    RRRRRRR*  RR      R   RR   RRRRR   RR  RRRRRRRR        
    RRRRRRR*  RR      R   RR  RR   R   RR     RR            
    RRRRRRR:  RR      R   RR  R        RR     RR            
    @RRRRRR   RR      R   RR  R  RRR:  RR     RR            
     RRRRR@   RR      R   RR  R+   R:  RR     RR            
      :RR     RRRRR   RR  RR  @R, +R:  RR     RR            
       RR     RRRRR    RRRR    #RRR#   RR     RR            
       RR                                           
       RR                                           
       R.                                           
        `;

        console.log(`${chalk.green(logo)}`);
    },
    log: (text) => {
        log(text);
    },

    error: (text) => {
        console.log(`${chalk.red(sig)} ${text}`);
    },

    warning: (text) => {
        console.log(`${chalk.yellow(sig)} ${text}`);
    },

    middleware: (req, res, next) => {

      let msg = `${req.method} ${req.url} ${chalkifyStatusCode(res.statusCode)}`;

      if (res.headersSent) {
          log(msg);
      } else {
        res.on('finish', function() {
          log(msg);
        })
      }
      next();
    }
}