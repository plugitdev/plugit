const tableNames = require('./constants/tableNames');
import { logger } from './logger';
const chalk = require('chalk');
const knex = require("knex");
const knexfile = require('../knexfile').development;
export class databaseConnection {
  knex: any;
  
  constructor(db = null) {
    if (!db || !db.client) {
      this.knex = knex(knexfile);
      return;
    } else {
      this.knex = knex(db);
    }
  }

  getSettings(callback) {
    this.knex("settings").first().then(callback).catch((e) => {
      console.log(e);
      if (e.code == "SQLITE_ERROR") logger.error(`There has been an error with the database, this likely means it hasn't been created. you can do this by running ${chalk.green('"npm run migrate"')}`); process.exit();
    });
  }

  createUser(info, callback) {
    this.knex(tableNames.user)
      .insert(info)
      .then((user) => {
        return callback(user);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getUser(discord_id, knex, callback=null) {
    this.knex("users")
      .where({ discord_ID: discord_id })
      .first()
      .then((user) => {
        if (callback) {
          callback(user);
        }
        return user;
      })
      .catch((err) => {
        console.log(err);
        return err;
      });

    return false;
  }


  updateGuilds() {
    //Update a users guildlist in the database.
  }

  updateUserInfo() {
    //Update a users info (based on discord id in the database)
  }

  updateAll() {
    this.updateGuilds();
    this,this.updateUserInfo();
  }
}