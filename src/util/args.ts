class argsParser {
    args: Array<string>;
    command: string;
    
    constructor() {

    }
  
    parse(msg, prefix) {
      this.args =  msg.content.slice(prefix).trim().split(/ +/g);
      this.command = this.args.shift().toLowerCase().split(prefix)[1];
      return ({
        args: this.args,
        command: this.command
    });
  }
}

export var args = new argsParser();
