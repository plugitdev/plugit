var getSettings = require("../helpers/getSettings");

var getPrefix = (() => {
    getSettings((data) => {
        return data['prefix'];
    });
});

module.exports = getPrefix;