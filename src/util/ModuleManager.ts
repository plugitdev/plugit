import { logger } from "./logger";
import { Constants, Message, MessageEmbed } from "discord.js";
import { Command, ModuleEvent, Param, PlugitModule } from "../interfaces";
import { Plugitbot } from "../bot";
import { promises as fs } from 'fs';
import * as chalk from 'chalk';
import * as path from 'path';
const pathSeperator = "/";
const modulesPath = path.join(__dirname + "/../modules/");

export class ModuleManager {
  public commands: object;
  public events: object;
  public modules: Array<PlugitModule>;
  public commons: Array<PlugitModule>;
  searchDirectory: string;
  settings: Array<any>;
  prefix: string;
  sig: string;

  constructor(searchDirectory: string = modulesPath) {
    this.commands = {};
    this.events = {};
    this.searchDirectory = searchDirectory;
    this.modules = [];
    this.settings = [];
    this.commons = [];
    this.sig = require("./constants/signature");
    this.prefix = "";
  }

  async run(that: Plugitbot, msg: Message, command: string) {
    this.commands[command].main(that.bot, that.db, msg, that.guildManager);
  }

  getProperUsageText(command: string) {
    let params = this.commands[command].parameters.params;
    let paramStr;

    if (params[0]) {
      paramStr = this.generateParamStr(command);
    } else {
      paramStr = params;
    }

    return `\nThe proper usage would be: ${this.prefix}${this.commands[command].name} ${paramStr}\``;
  }

  private generateParamStr(command: string) {
    let paramStr = "";

    this.commands[command].parameters.params.forEach((parameter: Param) => {
      paramStr += `${parameter.name || parameter} `;
    });
    return paramStr;
  }

  getCommandParameters(command: string) {
    return this.commands[command].parameters;
  }

  async doesCommandRequireParams(command: string, args) {
    let commandParams = this.getCommandParameters(command).params;
    let areAllParametersRequired = this.getCommandParameters(command).required;

    if (areAllParametersRequired) {
      return areAllParametersRequired && !(args.length == commandParams.length);
    } else {
      if (areAllParametersRequired == false || !commandParams) return;
      let isMissingParam = false;

      commandParams.forEach((param: Param, i) => {
        if (param.required == true) {
          if (!args[i]) {
            isMissingParam = true;
            return;
          }
        }
      });

      return isMissingParam;
    }
  }

  addCommand(command: Command) {
    this.commands[command.name] = command;
  }

  addEvent(event: ModuleEvent, key) {
    let isValidEvent = Object.keys(Constants.Events).find((event) => {
      return event == key;
    });

    if (isValidEvent) {
      event.event = Constants.Events[isValidEvent];
      this.events[key] = event;
    }
  }

  addHelpFieldToEmbed(embed: MessageEmbed, command: string) {
    let commandParams = this.getCommandParameters(command);
    let paramStr: string = "";

    if (!this.commands[command].name) {
      logger.error(`Command: ${command}. has no name field set.`);
      return;
    }
    if (!this.commands[command].help) {
      logger.warning(`Command: ${command}. has no help field set.`);
      return;
    }
    if (!commandParams || !commandParams.params) {
      logger.warning(`Command: ${command}. has no parameter field set.`);
    } else {
      if (commandParams.params[0]) {
        if (commandParams.params[0].name) {
          paramStr = this.generateParamStr(command);
        }
      }
    }

    paramStr = paramStr ? paramStr : commandParams.params;
    if (commandParams.params) {
      embed.addField(
        this.prefix + this.commands[command].name + " " + paramStr,
        this.commands[command].help,
        true
      );
    } else {
      embed.addField(
        this.prefix + this.commands[command].name,
        this.commands[command].help,
        true
      );
    }
  }

  private addCommandsAndEvents(module: PlugitModule) {
    if (module.commands && module.events) {
      for (var command in module.commands) {
        this.addCommand(module.commands[command]);
      }

      for (let event in module.events) {
        this.addEvent(module.events[event], event);
      }
    }
  }

  async loadCommon(path) {
    var module = require(path);
    var stat = await fs.lstat(path);
    path = path.split(pathSeperator);
    path = path[path.length - 1];

    this.commons.push(path);

    this.addCommandsAndEvents(module);
  }

  async loadModule(path: string) {
    var module: PlugitModule = require(path);
    var stat = await fs.stat(path);

    if (stat.isDirectory()) {
      try {
        var info = require(path + pathSeperator + "package.json");
        this.modules.push(info);
      } catch (e) {
        console.log(e);
        logger.warning(`Failed To Load Module: ${path}`);
      }
    }

    this.addCommandsAndEvents(module);
  }

  async loadModules() {
    var files = await fs.readdir(this.searchDirectory);
    for (var file in files) {
      var fileType = files[file]
        .toString()
        .substr(files[file].length - 3, files[file].length);

      if (fileType === ".js") {
        await this.loadCommon(path.join(this.searchDirectory + files[file]));
      } else {
        await this.loadModule(path.join(this.searchDirectory + files[file]));
      }
    }
  }

  async listModules() {
    logger.log(chalk.yellow("Installed Commons: "));
    this.commons.forEach((common) => {
      logger.log(" ├── " + chalk.green(common));
    });

    logger.log(chalk.yellow("Installed Modules: "));
    this.modules.forEach((modules) => {
      logger.log(" ├── " + chalk.green(modules.name));
    });
  }
}
