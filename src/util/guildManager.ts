import * as tableNames from "./constants/tableNames";
import { Guild } from "discord.js";
import { logger } from "../util/";
import { databaseConnection } from "./databaseConnection";

export class guildManager {
  db: databaseConnection;

  constructor(db = undefined) {
    if (db) { this.db = db; }
    else {
      this.db = new databaseConnection();
    }
  }

  updateAllServers(bot) {
    let guilds: Array<Guild> = bot.guilds.cache.array();
    guilds.forEach((guild) => {
      this.guildUpdate(guild);
    });
  }

  guildCreate(guild) {
    this.createServer(guild);
  }

  getGuild(guild, callback) {
    this.db.knex(tableNames.server)
      .where({ discord_ID: guild.id })
      .first()
      .then(callback)
      .catch((e) => {
        logger.warning(`Could not load guild: ${guild.name}`);
        console.log(e.stack);
      });
  }

  guildUpdate(guild) {
    this.getGuild(guild, (server) => {
      if (!server) {
        this.createServer(guild);
      } else {
        this.updateServer(guild);
      }
    });
  }

  createServer(guild, db: any = undefined) {
    if (db == undefined) {
      this.db.knex(tableNames.server)
        .insert({
          name: guild.name,
          discord_ID: guild.id,
          memberCount: guild.memberCount,
          icon: guild.iconURL
        })
        .catch(function (e) {
          console.log(e);
        });
    } else {
      db(tableNames.server)
        .insert({
          name: guild.name,
          discord_ID: guild.id,
          memberCount: guild.memberCount,
          icon: guild.iconURL
        })
        .catch(function (e) {
          console.log(e);
        });
    }
  }

  updateServer(guild, db = undefined) {
    db = db ? db : this.db.knex;
    db(tableNames.server)
      .where({ discord_ID: guild.id })
      .update(
        {
          name: guild.name,
          nameAcronym: guild.nameAcronym,
          discord_ID: guild.id,
          memberCount: guild.memberCount,
          icon: guild.iconURL,
        },
        ["name", "nameAcronym", "id", "memberCount", "icon"]
      )
      .catch(function (e) {
        logger.warning(`Error updating guild: ${guild.name}`);
      });
  }
}
