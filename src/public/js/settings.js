$(function () {
  const defaults = [$("#prefix").val(), $("#token").val(), $("#message").val()];

  $(".btn-success").click(() => {
    const data = {
        prefix: $("#prefix").val(),
        token: $("#token").val(),
        playing: $('#playing').val(),
        SESSION_SECRET: $('#SESSION_SECRET').val()
    };
    
    var xhr = new XMLHttpRequest();
    xhr.open("POST", '/settings/update/', true);
    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");

    // send the collected data as JSON
    xhr.send(JSON.stringify(data));

    xhr.onloadend = function () {
      // done
    };
  });
});
