var express = require("express");
var { isAuth } = require("../helpers/index");

const knex = require("knex")(require("../knexfile").development);
const util = require("../util");
function delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

export class SettingsController {
  public path = "/settings";
  public router = express.Router();

  constructor() {
    this.intializeRoutes();
  }

  public intializeRoutes() {
    this.router.post(this.path + "/update/", (req, res) => {
      // Update Items In Database.
      knex("settings")
        .first()
        .update(req.body)
        .then((data) => {
          res.redirect("/settings");
        })
        .catch((err) => {
          console.log(err);
        });
    });

    this.router.get(this.path + "/restart", async function (req, res, next) {
      util.logger.log(`Restarting Plugit Via Url`);
      req.app.locals.bot.restart();
      req.app.set("user", undefined);
      res.redirect('/');
      await delay(500);
      return process.exit();
    });

    this.router.get(this.path + "/stop", function (req, res, next) {
      util.logger.log(`Stopping Plugit Via Url`);
      res.redirect('/settings');
      req.app.locals.bot.bot.destroy();
      req.app.locals.bot.bot.user = undefined;
    });

    this.router.get(this.path + "/start", function (req, res, next) {
      if (req.app.locals.bot.bot) {
        util.logger.warning('Cant start bot is already running.')
      } else {
        util.logger.log(`Restarting Plugit Via Url`);
        res.redirect('./settings');
        req.app.locals.bot.bot.destroy();
      }

    });

    this.router.get(this.path + "/", function (req, res, next) {
      knex("settings")
        .first()
        .then(async (data) => {
          var { token, prefix, clientid, clientsecret, playing, SESSION_SECRET } = data;
          clientsecret = clientsecret || process.env.CLIENT_SECRET;
          res.render("settings", {
            title: "Plugit - Settings",
            token: process.env.token || token,
            tokenReadOnly: process.env.token ? true : false,
            botTag: (req.app.locals.bot.bot.user) ? req.app.locals.bot.bot.user.tag : 'NULL',
            botAvatar: (req.app.locals.bot.bot.user) ? req.app.locals.bot.bot.user.avatarURL() : '',
            prefix: process.env.prefix || prefix,
            prefixReadOnly: process.env.prefix ? true : false,

            clientid: process.env.CLIENT_ID || clientid,
            clientidReadOnly: process.env.CLIENT_ID ? true : false,

            clientsecret: process.env.CLIENT_SECRET || clientsecret,
            clientsecretReadOnly: process.env.CLIENT_SECRET ? true : false,

            SESSION_SECRET: process.env.SESSION_SECRET || SESSION_SECRET,
            SESSION_SECRET_ISREADONLY: process.env.SESSION_SECRET ? true : false,

            playing: process.env.playing || playing,
            playingReadOnly: process.env.playing ? true : false,
            settings: [
              {
                id: 1,
                icon: "cog",
                name: "General Settings",
                active: true,
              },
              {
                id: 2,
                icon: "globe",
                name: "Connection Settings",
              },
              {
                id: 3,
                icon: "plug",
                name: "Plugin Manager",
              },
            ],
            tabid: 1,
          });
        });
    });
  }
}
