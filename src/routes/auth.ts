var express = require("express");
var passport = require("passport");
var { isAuth } = require("../helpers/isAuth");

export class AuthController {
  public path = "/auth";
  public router = express.Router();

  constructor() {
    this.intializeRoutes();
  }

  public intializeRoutes() {
    this.router.get(this.path + "/", passport.authenticate("discord"));

    this.router.get(this.path + "/logout", (req, res) => {
      if (req.user) {
        req.logout();
        res.redirect("/");
      } else {
        res.redirect("/");
      }
    });

    /* GET auth listing. */
    this.router.get(
      this.path + "/redirect",
      isAuth,
      passport.authenticate("discord", {
        failureRedirect: "/forbidden",
      }),
      (req, res) => {
        req.app.set("user", req.user);
        res.redirect("/");
      }
    );
  }
}
