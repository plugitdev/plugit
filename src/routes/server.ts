var express = require("express");
var { isAuth } = require("../helpers/isAuth");
const knex = require("knex")(require("../knexfile").development);

export class ServerController {
  public path = "/server";
  public router = express.Router();

  constructor() {
    this.intializeRoutes();
  }

  public intializeRoutes() {
    this.router.get(this.path, isAuth, (req, res, next) => {
      res.redirect("/");
    });

    this.router.get(this.path + "/:server", (req, res, next) => {
      knex("servers")
        .where({ discord_ID: req.params.server })
        .first()
        .then((server) => {
          res.render("server", {
            title: `Pluit - ${server.name}`,
            server: server,
            botTag: (req.app.locals.bot.bot.user) ? req.app.locals.bot.bot.user.tag : 'NULL',
            botAvatar: (req.app.locals.bot.bot.user) ? req.app.locals.bot.bot.user.avatarURL() : '',
          });
        })
        .catch((err) => {
          console.log(err);
        });
    });
  }
}
