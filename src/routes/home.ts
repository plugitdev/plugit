import * as express from 'express';
const isAuth = require("../helpers/isAuth");
const knex = require("knex")(require("../knexfile").development);
var passport = require("passport");
export class HomeController {
  public path = "/";
  public router = express.Router();

  constructor() {
    this.intializeRoutes();
  }

  public intializeRoutes() {
    /* GET home page. */
    this.router.get(this.path, (req, res, next) => {
      req.user = req.app.get("user");
      if (!req.user) {
        res.render("index", { title: "Plugit - Sign In!" });
      } else {
        knex("servers")
          .then((servers) => {
            knex("users")
              .where({ id: req.user.id })
              .first()
              .then((user) => {
                let servers_to_render = [];
                let guilds = JSON.parse(user.guilds);

                for (let server of servers) {
                  if (guilds.includes(server.discord_ID)) {
                    servers_to_render.push(server);
                  }
                }
                res.render("dashboard", {
                  servers: servers_to_render,
                  botTag: (req.app.locals.bot.bot.user) ? req.app.locals.bot.bot.user.tag : 'NULL',
                  botAvatar: (req.app.locals.bot.bot.user) ? req.app.locals.bot.bot.user.avatarURL() : '',
                });
              })
              .catch((err) => {
                console.log(err);
              });
          })
          .catch((err) => {
            console.log(err);
          });
      }
    });
  }
}