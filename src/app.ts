import { development } from "./knexfile";
import { Chalk as chalk } from "chalk";
import * as Knex from "knex";
import * as util from "./util";
import * as express from "express";
import * as path from "path";
import * as cookieParser from "cookie-parser";
import * as passport from "passport";
import { PlugitPassportStrategy } from './passport/discord';

import { HomeController, ServerController, AuthController, SettingsController } from "./routes/";
var KnexSessionStore;

util.logger.logo();

class PlugitWebServer {
  app: any;
  bot: any;
  store: any;
  db: any;
  session: any;

  constructor() {
    this.app = express();
    this.bot = this.app.locals.bot;
    this.db = new util.databaseConnection(development);

    this.session = require("express-session");
    this.initMiddleware();
    KnexSessionStore = require("connect-session-knex")(this.session);

    this.store = new KnexSessionStore({
      knex: this.db.knex,
      tablename: "sessions",
    });

    this.initPassport();
    this.errorHandlers();
    this.initializeControllers();
  }

  initPassport() {
    this.app.use(passport.initialize());
    this.app.use(passport.session());
    new PlugitPassportStrategy();
  }

  initMiddleware() {
    this.app.set("views", path.join(__dirname, "views"));
    this.app.set("view engine", "pug");
    this.app.use(util.logger.middleware);

    this.db.getSettings((data) => {
      if (!process.env.SESSION_SECRET && !data['SESSION_SECRET']) {
        util.logger.error("Please set a SESSION_SECRET using the .env file")
        return;
      }

      this.app.use(
        this.session({
          secret: process.env.SESSION_SECRET || data['SESSION_SECRET'],
          resave: true,
          cookie: {
            maxAge: 60000 * 60 * 24,
          },
          store: this.store,
          saveUninitialized: false,
        })
      );
    })

    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));
    this.app.use(cookieParser());
    this.app.use(express.static(path.join(__dirname, "public")));
  }

  private initializeControllers() {
    this.app.use("/", new AuthController().router);
    this.app.use("/", new HomeController().router);
    this.app.use("/", new ServerController().router);
    this.app.use("/", new SettingsController().router);
  }

  errorHandlers() {
    // error handler
    this.app.use(function (err, req, res, next) {
      // set locals, only providing error in development
      res.locals.message = err.message;
      res.locals.error = req.app.get("env") === "development" ? err : {};

      // render the error page
      res.status(err.status || 500);
      res.render("error");
    });
  }
}

let server = new PlugitWebServer().app;

module.exports = server;
