const passport = require("passport");
const knexfile = require("../knexfile").development;
const { Permissions } = require("discord.js");
const DiscordStrategy = require("passport-discord").Strategy;
import { databaseConnection, logger } from '../util';
const util = require("../util");

let clientid;
let clientsecret;

export class PlugitPassportStrategy {
  database: databaseConnection;
  config: object;
  db: any;

  constructor() {
    this.db = require("knex")(knexfile);
    this.database = new databaseConnection(knexfile);
    this.database.getSettings((data) => {
      clientid = process.env.CLIENT_ID || data["clientid"];
      clientsecret = process.env.CLIENT_SECRET || data["clientsecret"];
    });
    this.config = {
      clientID: clientid || process.env.CLIENT_ID,
      clientSecret: clientsecret || process.env.CLIENT_SECRET,
      callbackURL: process.env.CLIENT_REDIRECT || "/auth/redirect",
      scope: ["identify", "guilds"],
    };
    this.initSerializeUser();
    this.initDeserializeUser();
    this.passportConfig();
  }

  initSerializeUser() {
    passport.serializeUser((user, done) => {
      done(null, user);
    });
  }

  initDeserializeUser = () => {
    passport.deserializeUser(async (user, done) => {
      var DBData = await this.database.getUser(user.id, this.db);

      if (DBData) {
        done(null, DBData);
      } else {
        done(DBData, null);
      }
    });
  }

  get_guilds(profile) {
    var guildsToDB = [];
    for (let guild of profile.guilds) {
      // Check If The User Has All Permissions On The Server
      const permissions = new Permissions(guild.permissions);

      if (permissions.has("ADMINISTRATOR")) {
        guildsToDB.push(guild.id);
      }
    }

    return JSON.stringify(guildsToDB);
  }

  passportConfig() {
    passport.use(
      new DiscordStrategy(
        this.config,
        (accessToken, refreshToken, profile, done) => {
          this.database.getUser(profile.id, this.db, (user) => {
            if (!user) {
              logger.log(`User Not Found. Creating user for ${profile.id}`);
              try {
                this.database.createUser(
                  {
                    discord_ID: profile.id,
                    name: profile.username,
                    guilds: this.get_guilds(profile),
                  },
                  (user) => {
                    user = user;
                    this.database.getUser(profile.id, this.db, (user) => {
                      return done(null, user);
                    });
                  }
                )
              } catch(e) {
                return done(e, false);
              }
            } else {
              return done(null, user);
            }
          });
        }
      )
    );
  }
}
