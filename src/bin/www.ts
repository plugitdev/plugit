/**
 * Module dependencies.
 */
import { Plugitbot } from '../bot';
var fs = require('fs');
var http = require('http');
var app = require('../app');
var util = require('../util');
var chalk = require('chalk');
var { ShardingManager } = require('discord.js');
var UTLIB = require('util');
var log_file = fs.createWriteStream(__dirname + '/debug.log', {flags : 'w'});
var log_stdout = process.stdout;


/**
 * Log to debug.log file.
 */

console.log = function(d) { //
  log_file.write(UTLIB.format(d) + '\n');
  log_stdout.write(UTLIB.format(d) + '\n');
};

/**
 * Get port from environment and store ifn Express.
 */

var port = normalizePort(process.env.PORT || '8080');
app.set('port', port);
app.locals.bot = new Plugitbot();;


/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
util.logger.log("Web Ui Started On: " + chalk.greenBright(`http://localhost:${port}`));

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}