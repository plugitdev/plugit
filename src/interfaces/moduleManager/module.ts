import { Command } from "..";

export interface PlugitModule {
    name: string
    commands: Array<Command>
    events: []
}