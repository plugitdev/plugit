export interface ModuleEvent {
    main: Function;
    event?: Object;
}