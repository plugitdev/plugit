import { Client, Message } from "discord.js";
import { databaseConnection } from "../../util";

export type CommandFunction = (
  bot: Client,
  db: databaseConnection,
  msg: Message
) => any;
export interface Command {
  name: string;
  parameters: {
    params: Array<any>;
    required?: boolean;
  };
  help: string;
  main: CommandFunction;
}
