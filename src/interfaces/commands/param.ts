export interface Param {
  name: string;
  required: boolean;
}
