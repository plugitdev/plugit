require("dotenv").config();

exports.development = {
  client: "sqlite3",
  useNullAsDefault: true,
  connection: {
    filename: "plugit.sqlite3",
  },
};
